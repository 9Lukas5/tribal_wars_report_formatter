# TribalWars Report Formatter

> [Zur Gebrauchsanweisung für Nutzer](Usage.md)

## Introduction

This project was done as a first experience using Rust as programming language as I planned on using it for something else and first wanted a comparably small CLI program, before jumping into a big framework in a new langauge.

This was done in ~1-2 weeks after reading through [The Rust Programming Language](https://doc.rust-lang.org/book/title-page.html).
I'd be happy about feedback to the coding I did with this in the issue tracker :)  
(I probably won't ever remove this note, as long as feedback for the project doesn't bother me even years later :p)

## About

This is a parser/forum BB-Code generator for battle reports of the german edition of the InnoGames browser game [_Die Stämme_](https://www.die-staemme.de/).
Input being mark and copy the display representation of a report in the browser and output being the corresponding forum BB-Code.

Reason for this tool is there can is a limited number of reports that can be directly shared with others using a free account.
By this more reports can be shared while also keeping the old reports in the forum history available without hitting this payment border^^

## Contributing

I'm a fan of [Conventional Commits](https://www.conventionalcommits.org/) and [Conventional Changelog](https://github.com/conventional-changelog/conventional-changelog) generated from them.
So, I'll demand you to write your commit messages accordingly.

Apart from that I encourage everyone to make use of the Issue tracker and merge request system.

The project uses the [Semantic Versioning](https://semver.org/) standard for its version numbers. 
