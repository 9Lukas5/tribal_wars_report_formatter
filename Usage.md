# Stämme Berichtsformatierer

## Nutzung

Das ist ein Desktop-Programm für das deutsche Online Browserspiel [_Die Stämme_](https://www.die-staemme.de/) von InnoGames,
um Kampfberichte in In-Game Forum-BB-Code zu konvertieren.

Das Programm ist ein reines Konsolen-Programm ohne grafisches Nutzerinterface.
Es kann auf zwei weisen genutzt werden:

- __Vom Terminal__  
    Hier sind auch weitere Befehle mit dem Paramter `--help` abrufbar
- __Öffnen einer Textdatei mit diesem Programm__  
    Dabei wird der Dateipfad der Textdatei an das Programm übergeben und es versucht einen Bericht aus deren Inhalt zu lesen.
    Der zugehörige BB-Code wird dann in einer neuen Datei im selben Verzeichnis gespeichert, deren Name mit `TribalWars-Report` beginnt.

## Empfehlung

- Programm herunterladen
- Textdatei mit neuer Dateiendung erstellen (zB. `Bericht-eingabe.staemme`)
- Eigenschaften dieser Datei öffnen und als Standardprogramm für Dateien dieser Endung den Formatierer festlegen
- Bericht kopieren und in diese Datei speichern
- Datei per Doppelklick "öffnen" => im selben Verzeichnis landet kurz später eine Textdatei die mit `TribalWars-Report` beginnt

## Was muss kopiert werden

Theoretisch geht es selbst, wenn im Browser bei geöffnetem Bericht einfach alles markiert wird.
Mindestens vorhanden sein muss aber der Text ab dem Wort __"Betreff"__ bis einschließlich dem unteren Satz __"Diesen Bericht veröffentlichen"__.

## Download

Die neueste Version kann immer auf der [Release-Seite](../../releases) dieses Repositories gefunden werden.
