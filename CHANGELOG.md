# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [1.0.1](../../compare/1.0.0...1.0.1) (2023-02-17)

## 1.0.0 (2023-02-15)


### Features

* initial release ([8f94e61](8f94e6199d0c30213c7891f0454684aeae175c04))

## 1.0.0 (2023-02-15)


### Features

* initial release ([1957db9](1957db9676fd12704499935c535e8090ebe6855f))
