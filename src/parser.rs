use regex::{Captures, Regex};
use crate::types;
use crate::types::{Report, ReportParserError, ReportType, Scouting, SearchState, Building};

#[cfg(test)]
pub mod tests;

/// Core function of the library: trying to form a [Report] instance from the given text
///
/// # Errors
///
/// Many things can go wrong here obviously.
/// If that's the case a [ReportParserError] is returned.
///
pub fn parse_report(text: &str) -> Result<Report, ReportParserError>
{
    use SearchState::*;

    let mut lines = text.lines();
    let mut report = Report::new();
    let mut state = Start;

    log::trace!("start processing input text");
    let mut line_option = lines.next();
    while line_option.is_some()
    {
        let line = line_option.unwrap();
        log::trace!("processing line: {}", line);

        log::trace!("checking for state changing keyword");
        if line.contains("Betreff") { state = Subject; }
        else if line.contains("Kampfzeit") { state = BattleTime; }
        else if line.contains("hat gewonnen") { state = WinnerString; }
        else if line.contains("Angreiferglück") { state = Luck; }
        else if line.contains("Moral:") { state = Moral; }
        else if line.contains("Nachtbonus") { state = NightBonus; }
        else if line.contains("Angreifer:") { state = Attacker; }
        else if line.contains("Herkunft:") { state = Source; }
        else if line.contains("Verteidiger:") { state = Defender; }
        else if line.contains("Ziel:") { state = Target; }
        else if line.contains("Anzahl:")
        {
            state = match state
            {
                Source => AttackerTroops,
                Target => DefenderTroops,
                _ => state,
            };
        }
        else if line.contains("Verluste:")
        {
            state = match state
            {
                AttackerTroops => AttackerLosses,
                DefenderTroops => DefenderLosses,
                _ => state,
            };
        }
        else if line.contains("Flagge:")
        {
            state = match state
            {
                AttackerLosses => AttackerFlag,
                Target | DefenderLosses => DefenderFlag,
                _ => state,
            };
        }
        else if line.contains("Effekte:")
        {
            state = match state
            {
                AttackerLosses | AttackerFlag => AttackerEffects,
                Target | DefenderLosses | DefenderFlag => DefenderEffects,
                _ => state,
            }
        }
        else if line.contains("Schaden durch Rammböcke:") { state = RamDamage; }
        else if line.contains("Schaden durch Katapultbeschuss:") { state = CatapultDamage; }
        else if line.contains("Spionage") { state = Scouting; }
        else if line.contains(" XP") { state = PaladinExp; }
        else if line.contains("Zustimmung") { state = Agreement; }
        else if line.contains("Diesen Bericht veröffentlichen") { state = Finished; }

        log::debug!("state is now: {}", state);

        match state
        {
            Start => (),
            Subject =>
                {
                    if let Some(text) = search_token(r"^Betreff\s+(.*)\s*$", line)
                    {
                        let text = text.get(1).unwrap().as_str().to_string();
                        log::debug!("found subject: {}", &text);
                        report.subject = text;

                        if report.subject.contains("greift")
                            && report.subject.ends_with("an")
                        {
                            report.report_type = ReportType::Attack;
                        } else if report.subject.contains("erobert")
                        {
                            report.report_type = ReportType::Nobilitation;
                        } else if report.subject.contains("späht")
                        {
                            report.report_type = ReportType::Scout;
                        } else
                        {
                            log::warn!("subject line didn't include any keyword for known types of reports");
                        }
                    }
                }
            BattleTime =>
                {
                     if let Some(text) = search_token(r"^Kampfzeit\s+(.*)\s*$", line)
                    {
                        let text = text.get(1).unwrap().as_str().to_string();
                        log::debug!("found battle-time: {}", text);
                        report.battle_time = text;
                    }
                }
            WinnerString =>
                {
                    if let Some(text) = search_token(r"^((.*)\s+hat gewonnen)\s*$", line)
                    {
                        let text = text.get(1).unwrap().as_str().to_string();
                        log::debug!("found winner-string: {}", text);
                        report.winner_string = text;
                    }
                }
            Luck =>
                {
                    let mut set_fn = |text: Captures|
                        {
                            let text = text.get(1).unwrap().as_str().to_string();
                            log::debug!("found luck: {}", text);
                            report.luck = text;
                        };

                    if let Some(text) = search_token(r"^\s*Glück\s+(\d+\.\d+%)\s*$", line)
                    {
                        set_fn(text);
                    }
                    else if let Some(text) = search_token(r"^\s*(-\d+\.\d+%)\s+(Pech).*$", line)
                    {
                        set_fn(text);
                    }
                }
            Moral =>
                {
                    if let Some(text) = search_token(r"^Moral:\s+(\d+%)\s*$", line)
                    {
                        let text = text.get(1).unwrap().as_str().to_string();
                        log::debug!("found moral: {}", text);
                        report.moral = text;
                    }
                }
            NightBonus =>
                {
                    if search_token(r"^Nachtbonus für Verteidiger$", line).is_some()
                    {
                        log::debug!("night bonus active");
                        report.night_bonus = true;
                    }
                }
            Attacker =>
                {
                    if let Some(text) = search_token(r"^Angreifer:\s+(.*)\s*$", line)
                    {
                        let text = text.get(1).unwrap().as_str().to_string();
                        log::debug!("found attacker: {}", text);
                        report.attacker = text;
                    }
                }
            Source | Target =>
                {
                    let pattern = match state
                    {
                        Source => r"^Herkunft:.*\((\d{3}\|\d{3})\).*$",
                        Target => r"^Ziel:.*\((\d{3}\|\d{3})\).*$",
                        _ => return Err(ReportParserError::UnexpectedState),
                    };

                    if let Some(text) = search_token(pattern, line)
                    {
                        let text = text.get(1).unwrap().as_str().to_string();
                        log::debug!("found {} coordinates: {}", state, text);

                        match state
                        {
                            Source => report.source = text,
                            Target => report.target = text,
                            _ => return Err(ReportParserError::UnexpectedState),
                        }
                    }
                }
            Defender =>
                {
                    if let Some(text) = search_token(r"^Verteidiger:\s+(.*)\s*$", line)
                    {
                        let text = text.get(1).unwrap().as_str().to_string();
                        log::debug!("found defender: {}", text);
                        report.defender = text;
                    }
                }
            AttackerTroops
            |AttackerLosses
            |DefenderTroops
            |DefenderLosses =>
                {
                    let pattern = match state
                    {
                        AttackerTroops | DefenderTroops => r"^Anzahl:\s+(.*)\s*$",
                        AttackerLosses | DefenderLosses => r"^Verluste:\s+(.*)\s*$",
                        _ => return Err(ReportParserError::ParseTroopsError),
                    };

                    if let Some(text) = search_token(pattern, line)
                    {
                        let text = text.get(1).unwrap().as_str();
                        log::debug!("found {} troops string: {}", state, text);
                        let troops = extract_troops(text)?;

                        match state
                        {
                            AttackerTroops => report.attacker_troops = troops,
                            AttackerLosses => report.attacker_losses = troops,
                            DefenderTroops => report.defender_troops = Some(troops),
                            DefenderLosses => report.defender_losses = Some(troops),
                            _ => return Err(ReportParserError::ParseTroopsError),
                        }
                    }
                }
            AttackerFlag | DefenderFlag =>
                {
                    if let Some(text) = search_token(r"^Flagge:\s+(\+\d+% .*)\s*$", line)
                    {
                        let text = text.get(1).unwrap().as_str().to_string();

                        log::trace!("found {} flag: {}", state, text);
                        match state
                        {
                            AttackerFlag => report.attacker_flag = Some(text),
                            DefenderFlag => report.defender_flag = Some(text),
                            _ => return Err(ReportParserError::UnexpectedState),
                        }
                    }
                }
            AttackerEffects | DefenderEffects =>
                {
                    if let Some(text) = search_token(r"^(Effekte:\s+)?(.*\+\d+%\s.*(stärke|schaden|Wirkung).*)\s*$", line)
                    {
                        let text = text.get(2).unwrap().as_str().to_string();

                        log::trace!("found {} effect: {}", state, text);

                        match state
                        {
                            AttackerEffects =>
                                {
                                    if report.attacker_effects.is_none()
                                    {
                                        report.attacker_effects = Some(String::new());
                                    }
                                    *report.attacker_effects.as_mut().unwrap() =
                                        (
                                            report.attacker_effects.as_ref().unwrap().to_string()
                                                + " "
                                                + &text
                                        ).trim().to_string();
                                },
                            DefenderEffects =>
                                {
                                    if report.defender_effects.is_none()
                                    {
                                        report.defender_effects = Some(String::new());
                                    }
                                    *report.defender_effects.as_mut().unwrap() =
                                        (
                                            report.defender_effects.as_ref().unwrap().to_string()
                                                + " "
                                                + &text
                                        ).trim().to_string();
                                },
                            _ => return Err(ReportParserError::UnexpectedState),
                        }
                    }
                }
            RamDamage | CatapultDamage =>
                {
                    let pattern = r"^Schaden durch (Rammböcke|Katapultbeschuss):\s+(\w+) beschädigt von Level (\d+) auf Level (\d+)\s*$";
                    if let Some(text) = search_token(pattern, line)
                    {
                        let building = text.get(2).unwrap().as_str();
                        let from_level = text.get(3).unwrap().as_str().parse::<u32>().unwrap();
                        let to_level = text.get(4).unwrap().as_str().parse::<u32>().unwrap();

                        log::trace!(
                            "found {} damage on {} from {} to {}",
                            state, building, from_level, to_level);

                        match state
                        {
                            RamDamage => report.ram_damage = Some(types::RamDamage {
                                from: from_level,
                                to: to_level }),
                            CatapultDamage => report.catapult_damage = Some(types::CatapultDamage {
                                building: building.to_string(),
                                from: from_level,
                                to: to_level }),
                            _ => return Err(ReportParserError::UnexpectedState),
                        }
                    }
                }
            Scouting
                | ScoutingResources
                | ScoutingBuildings
                | ScoutingTroops => parse_scouting(&mut state, line, &mut report)?,
            PaladinExp =>
                {
                    if let Some(text) = search_token(r"^\s*(\w+)\s+((\d+(,|.))?\d+ XP)\s*$", line)
                    {
                        let name = text.get(1).unwrap().as_str().to_string();
                        let exp = text.get(2).unwrap().as_str().to_string();
                        log::trace!("found Paladin {} gained {}", name, exp);

                        match &mut report.paladin_experience_gain
                        {
                            Some(list) => list.push((name, exp)),
                            _ => report.paladin_experience_gain = Some(vec![(name, exp)]),
                        }
                    }
                }
            Agreement =>
                {
                    if let Some(text) = search_token(r"^Zustimmung:\s+Gesunken von (\d+) auf (\d+)\s*$", line)
                    {
                        let from = text.get(1).unwrap().as_str().parse::<u32>().unwrap();
                        let to = text.get(2).unwrap().as_str().parse::<u32>().unwrap();

                        log::trace!("found agreement drop from {} to {}", from, to);

                        report.agreement = Some((from, to));
                    }
                }
            Finished =>
                {
                    log::debug!("parsing finished, returning report instance: {:?}", &report);
                    return Ok(report);
                }
        }

        line_option = lines.next();
    }

    log::warn!("No more input lines left, but didn't reach finished state. Last state was: {}", state);
    Ok(report)
}

/// Creates a regex with the given string and matches it onto the second
///
/// # Parameters
///
/// - `regex` - takes the regular expression to be searched for. Ex: `r"^Hello.*$"`
/// - `line` - the line to match the regex onto
///
fn search_token<'a>(regex: &str, line: &'a str) -> Option<Captures<'a>>
{
    let re = Regex::new(regex).unwrap();
    re.captures(line)
}

/// Build a vector of troop counts found in a line
///
/// # Description
///
/// A line with numbers split by whitespaces appears in reports multiple times
/// representing the count of the individual troop kind involved for each opponent.
/// This function takes the whole line and converts it into an [Vec<u32>].
/// Which number stands for which unit type is defined by its position which is fixed.
///
/// # Parameters
///
/// - `text` - needs to be a line of text containing only integers split by whitespaces, nothing else
///
/// # Errors
///
/// [ReportParserError::ParseTroopsError] - If after the split on whitespace characters
///     any of the resulting parts fails to parse into an integer.
///
fn extract_troops(text: &str) -> Result<Vec<u32>, ReportParserError>
{
    match text
        .replace("\t", " ")
        .replace("  ", " ")
        .trim()
        .split(' ')
        .map(|t| t.parse::<u32>())
        .collect::<Result<Vec<u32>, _>>()
    {
        Ok(troops) => Ok(troops),
        Err(_) => Err(ReportParserError::ParseTroopsError),
    }
}

/// Internal method for the parsing logic for the scouting part of a report
///
/// # Description
///
/// This method has to be called multiple times with different lines to finally get the full
/// data of a scouting parsed out.
/// It takes in the current state and report instance and extends it with whatever information
/// of a scouting part can be found in the given line.
///
/// # Parameters
/// - `state` - current state from the main parsing function as mutual reference
/// - `line` - the current processed line
/// - `report` - the currently built report instance to store eventual findings in
///
/// # Errors
///
/// - [ReportParserError::UnexpectedState] in case this function got called with a state not
///     designated to the scouting part of a report
///
fn parse_scouting(state: &mut SearchState, line: &str, report: &mut Report) -> Result<(), ReportParserError>
{
    use SearchState::{ScoutingResources, ScoutingBuildings, ScoutingTroops};

    log::trace!("checking for state changing keyword");
    if line.contains("Erspähte Rohstoffe:") { *state = ScoutingResources; }
    else if line.contains("Gebäude") { *state = ScoutingBuildings; }
    else if line.contains("Einheiten außerhalb") { *state = ScoutingTroops; }

    log::debug!("state is now: {}", state);

    match state
    {
        SearchState::Scouting => report.scouting = Some(Scouting::new()),
        ScoutingResources =>
            {
                if let Some(text) = search_token(r"^Erspähte Rohstoffe:\s+((\d+\.)?\d+)\s((\d+\.)?\d+)\s((\d+\.)?\d+)\s*$", line)
                {
                    let wood = text.get(1).unwrap().as_str().to_string();
                    let stone = text.get(3).unwrap().as_str().to_string();
                    let iron = text.get(5).unwrap().as_str().to_string();

                    log::debug!("found {} wood, {} stone and {} iron", wood, stone, iron);
                    report.scouting.as_mut().unwrap().resources = Some((wood, stone, iron));
                }
            }
        ScoutingBuildings =>
            {
                if let Some(text) = search_token(r"^\s*(\w+)\s+(\d+)\s*$", line)
                {
                    let building = text.get(1).unwrap().as_str().to_string();
                    let level = text.get(2).unwrap().as_str().parse::<u32>().unwrap();

                    log::debug!("found {} on level {}", building, level);

                    if let Some(buildings) = &mut report.scouting.as_mut().unwrap().buildings
                    {
                        buildings.push(Building::try_from((&building, level))?);
                    } else
                    {
                        report.scouting.as_mut().unwrap().buildings = Some(vec![Building::try_from((&building, level))?]);
                    }
                }
            }
        ScoutingTroops =>
            {
                if let Some(text) = search_token(r"^\s*(\d+(\s+\d+)+)\s*$", line)
                {
                    let text = text.get(1).unwrap().as_str();
                    log::debug!("found external troops string: {}", text);
                    report.scouting.as_mut().unwrap().ext_troops = Some(extract_troops(text).unwrap());
                }
            }
        _ => return Err(ReportParserError::UnexpectedState),
    }

    Ok(())
}
