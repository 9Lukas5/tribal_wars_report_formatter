
//! # TribalWars Report Formatter
//!
//! [tribal_wars_report_formatter][crate] is for the german browser game Triablwars, to convert the
//! raw text from a battle report copied straight out of the browser into forum fitting BB-Code
//!

/// Holds all type definitions publicly needed
///
/// Central type is the [crate::types::Report] struct.
/// This can be instantiated with default values for all fields, or by handing in a [&str]
/// to parse the data from.
///
/// # Usage
///
/// New empty instance:
/// ```
/// use tribal_wars_report_formatter::types::Report;
/// let report = Report::new();
/// ```
///
/// Create instance from String data:
/// ```
/// use tribal_wars_report_formatter::types::Report;
/// let report = Report::try_from("<input data>");
/// ```
///
pub mod types;

mod parser;
mod bbcode;
