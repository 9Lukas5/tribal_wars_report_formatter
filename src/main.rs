use std::fs;
use std::io;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use clap::{ArgMatches, crate_version};

use log::{LevelFilter};
use simple_logger::SimpleLogger;

use crate::types::{Report, ReportParserError};
use crate::cli_menu::{ArgumentIds, build_cli_menu};

mod bbcode;
mod cli_menu;
mod parser;
mod types;

#[cfg(test)]
mod bin_tests;

fn main()
{
    let matches = build_cli_menu();

    SimpleLogger::new()
        .with_local_timestamps()
        .with_level(match matches.get_one::<u8>(ArgumentIds::Verbosity.as_str()).unwrap()
        {
            0 => LevelFilter::Off,
            1 => LevelFilter::Error,
            2 => LevelFilter::Warn,
            3 => LevelFilter::Info,
            4 => LevelFilter::Debug,
            _ => LevelFilter::Trace,
        })
        .init()
        .unwrap();
    log::info!("Initialized Logger");
    log::info!("TribalWars report formatter {}", crate_version!());

    let report: Result<Report, ReportParserError>;
    let path_buf;
    let mut in_path: Option<&Path> = None;

    if let Some(text) = matches.get_one::<String>(ArgumentIds::Raw.as_str())
    {
        report = Report::try_from(text.as_str());
    } else if let Some(p) = matches.get_one::<String>(ArgumentIds::Infile.as_str())
    {
        path_buf = PathBuf::from(p);
        in_path = Some(path_buf.as_path());

        if !in_path.unwrap().is_file()
        {
            log::error!("given path is not a file: {}", in_path.unwrap().to_str().unwrap());
            std::process::exit(-1);
        }

        match fs::File::options()
            .read(true)
            .write(false)
            .open(in_path.unwrap())
        {
            Ok(mut file) =>
                {
                    let mut input = String::new();
                    if let Err(err) = file.read_to_string(&mut input)
                    {
                        log::error!("reading file failed: {}", err);
                    }

                    report = Report::try_from(input.as_str());
                }
            Err(err) => match err.kind()
            {
                io::ErrorKind::NotFound =>
                    {
                        log::error!("couldn't find given file {}", err);
                        std::process::exit(-1);
                    }
                io::ErrorKind::PermissionDenied =>
                    {
                        log::error!("permission to open given file was denied: {}", err);
                        std::process::exit(-1);
                    }
                _ =>
                    {
                        log::error!("Something unforeseen went wrong opening the file: {}", err);
                        std::process::exit(-1);
                    }
            }
        }

        in_path = in_path.unwrap().parent();

    } else
    {
        log::error!("No input data was given! Nothing to do here, bye o/");
        std::process::exit(-1);
    }

    let report = match report
    {
        Ok(value) => value,
        Err(err) =>
            {
                log::error!("Couldn't parse report from given input: {}", err);
                std::process::exit(-1);
            }
    };

    if let Err(err) = report.to_bbcode()
    {
        log::error!("Could not generate Forum BB-Code: {}", err);
        std::process::exit(-1);
    }

    if *matches.get_one::<bool>(ArgumentIds::Print.as_str()).unwrap_or(&false)
    {
        println!("{}", report.to_bbcode().unwrap());
    }

    if !*matches.get_one::<bool>(ArgumentIds::NoOut.as_str()).unwrap_or(&false)
    {
        write_report_to_file(&matches, &report, &in_path);
    }
}

/// Takes an [Report] instance and writes its bb-code representation into a file
///
/// ## Parameters
/// - `matches` - the [ArgMatches] instance of the CLI arguments the program got called with
/// - `report` - [Report] instance containing report data to be written to file
/// - `target_folder` - optional [Path] of a folder to write the file to. Only used if no explicit outfile was given through a CLI-argument!
///
/// ## Description
/// If the CLI-arguments include an explicit path setting this is used as output file,
/// otherwise the Filename is inherited from the contents of the [Report] instance in the form
/// `TribalWars-Report_<battle time in format 20yy-mm-ddThh-mm-ss-ms>_<subject stripped of possibly bad characters>`
///
fn write_report_to_file(matches: &ArgMatches, report: &Report, target_folder: &Option<&Path>)
{
    let mut out_path;

    if let Some(outfile) = matches.get_one::<String>(ArgumentIds::Outfile.as_str())
    {
        out_path = PathBuf::from(outfile);
    } else {
        let re = regex::Regex::new(r"^.*(\d{2})\.(\d{2})\.(\d{2}).*(\d{2}):(\d{2}):(\d{2}):(\d{3}).*$").unwrap();

        let timestamp = match re.captures(&report.battle_time)
        {
            Some(cap) => String::from("20")
                + cap.get(3).unwrap().as_str() + "-"
                + cap.get(2).unwrap().as_str() + "-"
                + cap.get(1).unwrap().as_str() + "-"
                + cap.get(4).unwrap().as_str() + "-"
                + cap.get(5).unwrap().as_str() + "-"
                + cap.get(6).unwrap().as_str() + "-"
                + cap.get(7).unwrap().as_str(),
            None =>
                {
                    log::warn!("Couldn't build timestamp for output filename from battle time! Using error text as time in filename");
                    "error-getting-timestamp".to_string()
                },
        };

        let name = report.subject.chars()
            .map(|c|
                {
                    return if c == ' '
                    {
                        '-'
                    } else {
                        c
                    }
                })
            .filter(|c| c.eq(&'-') || c.is_ascii_alphabetic() || c.is_ascii_digit())
            .collect::<String>();

        out_path = PathBuf::new();
        out_path.push(target_folder.unwrap_or(PathBuf::from(".").as_path()));
        out_path.push(String::from("TribalWars-Report_") + &timestamp + "_" + &name);
        out_path.set_extension("txt");
    }

    let file = fs::File::options()
        .write(true)
        .create_new(true)
        .open(out_path);

    match file
    {
        Ok(mut file) =>
            {
                match report.to_bbcode()
                {
                    Ok(bbcode) => if let Err(err) = file.write_all(bbcode.as_bytes())
                    {
                        log::error!("Failed writing to file: {}", err);
                        std::process::exit(-1);
                    },
                    Err(err) =>
                        {
                            log::error!("Failed to generate Forum-BBCode: {}", err);
                            return;
                        }
                }
            },
        Err(err) => match err.kind()
        {
            io::ErrorKind::AlreadyExists =>
                {
                    log::error!("target output file already exists: {}", err);
                    return;
                }
            io::ErrorKind::PermissionDenied =>
                {
                    log::error!("permission to open given file was denied: {}", err);
                    return;
                }
            _ =>
                {
                    log::error!("Something unforeseen went wrong opening the file: {}", err);
                    return;
                }
        }
    }
}
