use std::fs;
use std::path::Path;
use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::process::Command;

#[test]
fn crash_with_no_arguments() -> Result<(), Box<dyn std::error::Error>>
{
    let mut cmd = get_cmd()?;

    cmd.assert()
        .failure()
        .stderr(predicate::str::contains("No input data was given"));

    Ok(())
}

#[test]
fn crash_with_folder_as_input_path() -> Result<(), Box<dyn std::error::Error>>
{
    let mut cmd = get_cmd()?;

    cmd.arg("--no-out")
        .arg("./");

    cmd.assert()
        .failure()
        .stderr(predicate::str::contains("given path is not a file"));

    Ok(())
}

#[test]
fn recognize_if_output_file_already_exists() -> Result<(), Box<dyn std::error::Error>>
{
    let path = Path::new("./test_recognize_if_output_file_already_exists");
    fs::remove_file(path).unwrap_or(());

    fs::File::options()
        .write(true)
        .create_new(true)
        .open(path)?;

    let mut cmd = get_cmd()?;

    cmd.arg("-o").arg(path.as_os_str());
    cmd.arg("--raw").arg(ATTACK_REPORT);

    cmd.assert()
        .stderr(predicate::str::contains("target output file already exists: File exists"));

    fs::remove_file(path).unwrap_or(());

    Ok(())
}

pub fn get_cmd() -> Result<Command, Box<dyn std::error::Error>>
{
    Ok(Command::cargo_bin("tribal_wars_report_formatter")?)
}

const ATTACK_REPORT: &str = "
Übersichten

Karte

Berichte

Nachrichten



Rangliste
(807.|44.122 P)

      Stamm

Profil

 300

Einstellungen




0x001 | Cheyenne Mountain 	(336|657) K63

	194012 		156924 		163887 		400000 		15649/20476


Berichte
Alle
Angriffe
Verteidigung
Unterstützung
Handel
Events
Sonstiges
Weitergeleitet
Öffentlich
Filter

Weiterleiten		Löschen
Betreff 	el Dante (| Agent Orange | (346|652) K63) greift 0x008 | Latona (342|657) K63 an
Kampfzeit 	13.02.23 13:25:32:865
9Lukas5 hat gewonnen
Angreiferglück
Pech

	Glück 	5.9%
Moral: 60%
Angreifer: 	el Dante
Herkunft: 	| Agent Orange | (346|652) K63

Anzahl: 	0	0	3629	15	1589	0	250	81	0	0
Verluste: 	0	0	3629	15	1589	0	250	81	0	0

Verteidiger: 	9Lukas5
Ziel: 	0x008 | Latona (342|657) K63

Anzahl: 	5330	5078	0	189	0	494	0	75	1	0
Verluste: 	1307	1245	0	46	0	121	0	18	0	0

Schaden durch Rammböcke: 	Wall beschädigt von Level 18 auf Level 15
Schaden durch Katapultbeschuss: 	Wall beschädigt von Level 15 auf Level 14

Bonnie 	380,130 XP

» Diesen Bericht veröffentlichen
Weiterleiten		Löschen

Serverzeit: 23:12:26 13/02/2023

";
