use crate::types::{Building, Report, ReportParserError, SearchState};

#[cfg(test)]
mod tests;

/// Generate in-game forum BB-Code from a report instance
///
/// # Description
///
/// This function takes a reference to a report and generates the corresponding
/// in-game forum BB-Code from it.
///
/// # Usage
///
/// This function can't be called from outside the library directly. It is basically
/// part of the [Report] logic and just being extracted into a separate module.
/// Instead call the method of the [Report] struct, which will then use this logic.
///
/// ```
/// use tribal_wars_report_formatter::types::Report;
///
/// let report = Report::new();
/// let bbcode = report.to_bbcode();
/// ```
///
/// # Errors
///
/// - [ReportParserError] If the data in the instance can't be interpreted
///
pub fn report_to_bbcode(report: &Report) -> Result<String, ReportParserError>
{
    log::trace!("generating forum bb-code...");
    let mut output = String::new();

    {
        log::trace!("add battle-time to output");
        let battle_time = &((report.battle_time
            .split(":")
            .map(|s|
                {
                    match s.len()
                    {
                        3 => String::from(":") + "[size=7][color=#868686]" + s + "[/color][/size]",
                        _ => String::from(":") +  s,
                    }
                })
            .collect::<String>())[1..]);
        output = output + "Kampzeit: " + battle_time + "\n";
    }

    log::trace!("add winner-string to output");
    output = output + "[b][size=15]" + &report.winner_string + "[/size][/b]\n\n";

    {
        log::trace!("determining luck color");
        let color = match report.luck.starts_with('-')
        {
            true => "#ff0000",
            false => "#00a500",
        };

        log::trace!("add luck to output");
        output = output
            + "[b][i]Angreiferglück[/i][/b]: [b][color=" + color + "]"
            + &report.luck
            + "[/color][/b]\n\n";
    }

    log::trace!("add moral to output");
    output = output + "[b][i]Moral[/i]: " + &report.moral + "[/b]\n";

    log::trace!("check for night-bonus");
    if report.night_bonus
    {
        log::trace!("night-bonus active, add to output");
        output += "[b][i]Nachtbonus für Verteidiger[/i][/b]";
    }

    log::trace!("add attacker table to output");
    output += &generate_troops_table(&report, SearchState::Attacker)?;

    log::trace!("add defender table to output");
    output += &generate_troops_table(&report, SearchState::Defender)?;

    {
        let mut ram_catapult_table: Option<String> = None;

        log::trace!("check for ram damage");
        if let Some(ram_damage) = &report.ram_damage
        {
            log::trace!("ram damage present");
            if ram_catapult_table.is_none()
            {
                ram_catapult_table = Some(String::new());
            }

            ram_catapult_table = Some(ram_catapult_table.unwrap()
                + &"[**]Schaden durch Rammböcke:[|][b]Wall[/b] beschädigt von Level [b]"
                + &ram_damage.from.to_string() + "[/b] auf Level [b]"
                + &ram_damage.to.to_string() + "[/b]"
            );
        }

        log::trace!("check for catapult damage");
        if let Some(catapult_damage) = &report.catapult_damage
        {
            log::trace!("catapult damage present");
            if ram_catapult_table.is_none()
            {
                ram_catapult_table = Some(String::new());
            }

            ram_catapult_table = Some(ram_catapult_table.unwrap()
                + "[**]Schaden durch Katapultbeschuss:[|]"
                + "[b]" + &catapult_damage.building + "[/b]"
                + " beschädigt von Level [b]" + &catapult_damage.from.to_string() + "[/b]"
                + " auf Level [b]" + &catapult_damage.to.to_string() + "[/b]",
            );
        }

        if let Some(ram_catapult_table) = ram_catapult_table
        {
            output = output + "[table]" + &ram_catapult_table + "[/table]\n";
        }
    }

    log::trace!("check for paladin experience gain");
    if let Some(paladin_experience_gain) = &report.paladin_experience_gain
    {
        log::trace!("found paladin experience gain");
        let mut text = String::new();

        for (name, gain) in paladin_experience_gain
        {
            log::trace!("writing bbcode for paladin: {}", name);
            text = text + "\n[**][unit]knight[/unit] [b]" +  name + "[/b][|]+ " + gain;
        }

        output = output + "[table]" + &text + "[/table]\n";
    }

    log::trace!("check for scouting");
    if let Some(scouting) = report.scouting.as_ref()
    {
        log::trace!("adding scouting");
        output += "[b][i]Spionage[/i][/b]";

        log::trace!("check for scouted resources");
        if let Some(resources) = scouting.resources.as_ref()
        {
            log::trace!("resources present");
            output = output
                + "[table][**]Erspähte Rohstoffe:"
                + "[|][building]wood[/building] " + &resources.0
                + " [building]stone[/building] " + &resources.1
                + " [building]iron[/building] " + &resources.2
                + "[/table]";
        }

        log::trace!("check for scouted buildings");
        if let Some(buildings) = scouting.buildings.as_ref()
        {
            log::trace!("buildings present");
            let mut text = String::new();
            for building in buildings
            {
                log::trace!("add building: {:?}", building);
                text = text
                    + "[*]" + get_building_bbcode(&building) + " " + building.name()
                    + "[|]" + &building.level().to_string() + "\n";
            }

            log::trace!("attach buildings table to output");
            output = output + "[table][**]Gebäude[||]Stufe[/**]\n" + text.trim() + "\n[/table]";
        }

        log::trace!("check for scouted outside troops");
        if let Some(troops) = scouting.ext_troops.as_ref()
        {
            log::trace!("outside troops present");

            log::trace!("generating unit icons bbcode");
            let mut text = String::from("[*]") + get_units_bbcode(troops.len() as u8)? + "\n";

            log::trace!("generating troops bbcode");
            text = text + "[*]" + &troops_vector_to_bbcode(Some(troops))?[3..];

            log::trace!("attach outside troops table to output");
            output = output + "\n[b]Einheiten außerhalb:[/b]\n[table]" + &text + "[/table]";
        }
    }

    log::trace!("check for agreement");
    if let Some((from, to)) = report.agreement
    {
        log::trace!("agreement dropped, adding to output");
        output = output
            + "\n[table][**]Zustimmung:[|]Gesunken "
            + "von [b]" + &from.to_string() + "[/b] "
            + "auf [b]" + &to.to_string() + "[/b]"
            + "[/table]";
    }

    log::trace!("add subject as spoiler title and wrapping generated output into it");
    output = String::from("[spoiler=") + &report.battle_time + " - " + &report.subject + "]\n"
        + &output
        + "\n[/spoiler]";

    log::trace!("returning generated bb-code string now");
    return Ok(output);
}

/// Generates a troops table BB-Code for the report
///
/// # Parameters
/// - `report` - currently serialized report
/// - `state` - Either [SearchState::Attacker] or [SearchState::Defender] to determine the few differing name fields
///
/// # Errors
///
/// - [ReportParserError::UnexpectedState] if `state` is anything other
///     than [SearchState::Attacker] or [SearchState::Defender]
///
fn generate_troops_table(report: &Report, state: SearchState) -> Result<String, ReportParserError>
{
    let title: &str;
    let name: &str;
    let target_name: &str;
    let village_name: &str;
    let unit_type_count = report.attacker_troops.len() as u8;
    let units: Option<&Vec<u32>>;
    let losses: Option<&Vec<u32>>;
    let flag: Option<&String>;
    let effects: Option<&String>;

    match state
    {
        SearchState::Attacker =>
            {
                title = "Angreifer:";
                name = &report.attacker;
                target_name = "Herkunft:";
                village_name = &report.source;
                units = Some(&report.attacker_troops);
                losses = Some(&report.attacker_losses);
                flag = report.attacker_flag.as_ref();
                effects = report.attacker_effects.as_ref();
            }
        SearchState::Defender =>
            {
                title = "Verteidiger:";
                name = &report.defender;
                target_name = "Ziel:";
                village_name = &report.target;
                units = match &report.defender_troops{ Some(v) => Some(&v), None => None };
                losses = match &report.defender_losses{ Some(v) => Some(&v), None => None };
                flag = report.defender_flag.as_ref();
                effects = report.defender_effects.as_ref();
            }
        _ => return Err(ReportParserError::UnexpectedState),
    }

    let mut upper_table = String::new();
    upper_table = upper_table + "[**]" + title + "[|][player]" + name + "[/player][/**]\n";

    upper_table = upper_table + "[*]" + target_name + "[|][coord]" + village_name + "[/coord]";

    let mut troops_table = String::new();
    {
        let unit_icons = get_units_bbcode(unit_type_count)?;
        troops_table = troops_table + "[*][|]" + unit_icons + "\n";
    }

    {
        log::trace!("generating troops bbcode");
        let troops = troops_vector_to_bbcode(units).unwrap_or(String::new());
        troops_table = troops_table + "[*]Anzahl:" + &troops + "\n";

        log::trace!("generating loosses bbcode");
        let troops = troops_vector_to_bbcode(losses).unwrap_or(String::new());
        troops_table = troops_table + "[*]Verluste:" + &troops + "\n";

        troops_table = String::from("[table]\n") + &troops_table + "[/table]";

        if units.is_none() | losses.is_none()
        {
            log::trace!("clearing troops table, because no troops are present");
            troops_table = String::new();

            log::trace!("adding no survivers string");
            upper_table += "[*][|]Keiner deiner Kämpfer ist lebend zurückgekehrt. \
                Es konnten keine Informationen über die Truppenstärke des Feindes erlangt werden.";
        }
    }

    upper_table = String::from("[table]\n") + &upper_table + "[/table]";

    let mut lower_table = String::new();
    if let Some(flag) = flag
    {
        lower_table = lower_table + "[*]Flagge:[|]" + &flag + "\n";
    }

    if let Some(effects) = effects
    {
        lower_table = lower_table + "[*]Effekte:[|]" + effects;
    }

    if flag.is_some() || effects.is_some()
    {
        lower_table = String::from("[table]\n") + &lower_table + "[/table]";
    }

    return Ok(upper_table + &troops_table + &lower_table + "\n");
}

/// Returns BB-Code for header row of the units icons in a troops table
///
/// # Description
///
/// The troops table has a header row where each kind of unit is represented by a icon
/// which can be used in the forum through the correct BB-Code tag.
/// This function returns this row based on the count how many different kinds of units
/// are needed.
///
/// There are different numbers of unit kinds depending on the world the report comes from
/// as not all worlds include archers.
///
/// # Parameters
///
/// - `count` - the number how many different units are needed.
///     Should be the number of one of the troops [Vector]'s
///
/// # Errors
///
/// - [ReportParserError::General] if the requested number of unit kinds is unknown
fn get_units_bbcode(count: u8) -> Result<&'static str, ReportParserError>
{
    match count
    {
        10 => Ok("[unit]spear[/unit][|]\
            [unit]sword[/unit][|]\
            [unit]axe[/unit][|]\
            [unit]spy[/unit][|]\
            [unit]light[/unit][|]\
            [unit]heavy[/unit][|]\
            [unit]ram[/unit][|]\
            [unit]catapult[/unit][|]\
            [unit]knight[/unit][|]\
            [unit]snob[/unit]"),
        _ => return Err(ReportParserError::General("Unknown unit type count".to_string())),
    }
}

/// Maps an troops vector to a BB-Code table row string
///
/// # Description
///
/// Takes an [Vector] of [u32] representing the numbers of a troop row in the troops table
/// and maps it into a string that represents a BB-Code table row
/// with each [Vector] item in its own column
///
/// # Parameters
///
/// - `troops` - Troops [Vector] in an [Option], as the defender part of a report is not always
///     available
///
/// # Errors
///
/// - [ReportParserError::ParseTroopsError] if the [Option] is [None]
///
fn troops_vector_to_bbcode(troops: Option<&Vec<u32>>) -> Result<String, ReportParserError>
{
    match troops
    {
        Some(troops) => Ok(troops.iter().map(|t| format!("[|]{t}")).collect::<String>()),
        None => Err(ReportParserError::ParseTroopsError),
    }
}

/// Get BB-Code of a buildings name
///
/// # Description
///
/// Copying out the text from a report only gives the alphabetical name of it,
/// but the forum also has icons for each building available as BB-Code tag.
/// This function matches the alphabetical display name of a building and returns
/// the corresponding BB-Code tag.
///
/// # Parameters
///
/// - `name` - display name representation of a building
///
/// # Errors
///
/// - [ReportParserError::PatternNotFound] in case for the given display name no BB-Code tag is known
///
fn get_building_bbcode(name: &Building) -> &'static str
{
    use Building::*;

    match name
    {
        Main(_) => "[building]main[/building]",
        Barracks(_) => "[building]barracks[/building]",
        Stable(_) => "[building]stable[/building]",
        Garage(_) => "[building]garage[/building]",
        Snob(_) => "[building]snob[/building]",
        Smith(_) => "[building]smith[/building]",
        Place(_) => "[building]place[/building]",
        Statue(_) => "[building]statue[/building]",
        Market(_) => "[building]market[/building]",
        Wood(_) => "[building]wood[/building]",
        Stone(_) => "[building]stone[/building]",
        Iron(_) => "[building]iron[/building]",
        Farm(_) => "[building]farm[/building]",
        Storage(_) => "[building]storage[/building]",
        Wall(_) => "[building]wall[/building]",
    }
}
