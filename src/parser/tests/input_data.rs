
pub const ATTACK_REPORT: &str = "
Übersichten

Karte

Berichte

Nachrichten



Rangliste
(807.|44.122 P)

      Stamm

Profil

 300

Einstellungen




0x001 | Cheyenne Mountain 	(336|657) K63

	194012 		156924 		163887 		400000 		15649/20476


Berichte
Alle
Angriffe
Verteidigung
Unterstützung
Handel
Events
Sonstiges
Weitergeleitet
Öffentlich
Filter

Weiterleiten		Löschen
Betreff 	el Dante (| Agent Orange | (346|652) K63) greift 0x008 | Latona (342|657) K63 an
Kampfzeit 	13.02.23 13:25:32:865
9Lukas5 hat gewonnen
Angreiferglück
Pech

	Glück 	5.9%
Moral: 60%
Angreifer: 	el Dante
Herkunft: 	| Agent Orange | (346|652) K63

Anzahl: 	0	0	3629	15	1589	0	250	81	0	0
Verluste: 	0	0	3629	15	1589	0	250	81	0	0

Verteidiger: 	9Lukas5
Ziel: 	0x008 | Latona (342|657) K63

Anzahl: 	5330	5078	0	189	0	494	0	75	1	0
Verluste: 	1307	1245	0	46	0	121	0	18	0	0

Schaden durch Rammböcke: 	Wall beschädigt von Level 18 auf Level 15
Schaden durch Katapultbeschuss: 	Wall beschädigt von Level 15 auf Level 14

Bonnie 	380,130 XP

» Diesen Bericht veröffentlichen
Weiterleiten		Löschen

Serverzeit: 23:12:26 13/02/2023

";

pub const SCOUT_REPORT_NO_SURVIVERS: &str = "



Übersichten

Karte

Berichte

Nachrichten



Rangliste
(800.|44.687 P)

      Stamm

Profil

 300

Einstellungen




0x107 | Castiana 	(334|653) K63

	6421 		17393 		28654 		50675 		4579/4904


Berichte
Alle
Angriffe
Verteidigung
Unterstützung
Handel
Events
Sonstiges
Weitergeleitet
Öffentlich
Filter

Weiterleiten		Löschen
Betreff 	9Lukas5 (0x001 | Cheyenne Mountain (336|657) K63) späht York (344|659) K63
Kampfzeit 	12.02.23 12:42:07:448
staublurch hat gewonnen
Angreiferglück
Pech

	Glück 	16.0%
Moral: 100%
Angreifer: 	9Lukas5
Herkunft: 	0x001 | Cheyenne Mountain (336|657) K63

Anzahl: 	0	0	0	20	0	0	0	0	0	0
Verluste: 	0	0	0	20	0	0	0	0	0	0

Verteidiger: 	staublurch (gelöscht)
Ziel: 	Bonusdorf (344|659) K63

Keiner deiner Kämpfer ist lebend zurückgekehrt. Es konnten keine Informationen über die Truppenstärke des Feindes erlangt werden.
Flagge: 	+8% Verteidigungsstärke
Effekte: 	Schwertkämpfer: +30% Verteidigungsstärke
Speerträger: +30% Verteidigungsstärke
+3% Wall-Wirkung
+50% Verteidigungsstärke von Gebäuden gegen Belagerung

» Diesen Bericht veröffentlichen
Weiterleiten		Löschen

Serverzeit: 12:36:19 14/02/2023

";

pub const ATTACK_NIGHT_SCOUT_AGREEMENT: &str = "



Übersichten

Karte

Berichte (2)

Nachrichten



Rangliste
(799.|44.795 P)

      Stamm (1)

Profil

 300

Einstellungen




0x107 | Castiana 	(334|653) K63

	740 		9947 		27106 		50675 		4579/4904


Berichte
Alle
Angriffe
Verteidigung
Unterstützung
Handel
Events
Sonstiges
Weitergeleitet
Öffentlich
Filter

Weiterleiten		Löschen
Betreff 	9Lukas5 (0x001 | Cheyenne Mountain (336|657) K63) greift Hastings (343|660) K63 an
Kampfzeit 	10.02.23 00:42:55:313
9Lukas5 hat gewonnen
Angreiferglück
Pech

	Glück 	10.5%
Moral: 100%
Nachtbonus für Verteidiger
Angreifer: 	9Lukas5
Herkunft: 	0x001 | Cheyenne Mountain (336|657) K63

Anzahl: 	0	0	0	20	0	300	0	0	0	1
Verluste: 	0	0	0	0	0	33	0	0	0	0

Verteidiger: 	staublurch (gelöscht)
Ziel: 	0x109 | Revanna (343|660) K63

Anzahl: 	18	42	0	0	0	22	0	0	0	0
Verluste: 	18	42	0	0	0	22	0	0	0	0
Flagge: 	+6% Verteidigungsstärke

Spionage
Erspähte Rohstoffe:	112.892 80.254 1.175
Gebäude 	Stufe
Hauptgebäude 	22
Kaserne 	17
Stall 	19
Werkstatt 	4
Adelshof 	1
Schmiede 	20
Versammlungsplatz 	1
Gebäude 	Stufe
Statue 	1
Marktplatz 	15
Holzfällerlager 	26
Lehmgrube 	25
Eisenmine 	26
Bauernhof 	28
Speicher 	24
Einheiten außerhalb:

0	0	0	0	0	0	0	0	0	0

Zustimmung: 	Gesunken von 74 auf 51

» Diesen Bericht veröffentlichen
Weiterleiten		Löschen

Serverzeit: 14:23:51 14/02/2023


";

pub const ATTACK_EFFECTS_ATTACK_FLAG_PALADIN: &str = "



Übersichten

Karte

Berichte (3)

Nachrichten



Rangliste
(799.|44.795 P)

      Stamm

Profil

 300

Einstellungen




0x107 | Castiana 	(334|653) K63

	12017 		11224 		28592 		50675 		4579/4904


Berichte
Alle
Angriffe
Verteidigung
Unterstützung
Handel
Events
Sonstiges
Weitergeleitet
Öffentlich
Filter

Weiterleiten		Löschen
Betreff 	Askanon (63:35:07 // - AggresivGameplay (357|630) K63) greift 0x008 | Latona (342|657) K63 an
Kampfzeit 	13.02.23 12:23:13:710
9Lukas5 hat gewonnen
Angreiferglück
-10.4% 	Pech

	Glück
Moral: 48%
Angreifer: 	Askanon
Herkunft: 	63:35:07 // - AggresivGameplay (357|630) K63

Anzahl: 	0	0	6260	0	3122	0	314	50	0	0
Verluste: 	0	0	6260	0	3122	0	314	50	0	0
Flagge: 	+3% Angriffsstärke
Effekte: 	Axtkämpfer: +30% Angriffsstärke
Leichte Kavallerie: +30% Angriffsstärke
Katapult: +5% Gebäudeschaden

Verteidiger: 	9Lukas5
Ziel: 	0x008 | Latona (342|657) K63

Anzahl: 	9110	8670	0	211	0	1587	0	84	2	0
Verluste: 	945	900	0	22	0	165	0	9	0	0
Effekte: 	Schwertkämpfer: +15% Verteidigungsstärke
Speerträger: +30% Verteidigungsstärke
+50% Verteidigungsstärke von Gebäuden gegen Belagerung

Schaden durch Rammböcke: 	Wall beschädigt von Level 18 auf Level 17

Damon 	686,380 XP
Bonnie 	686,380 XP

» Diesen Bericht veröffentlichen
Weiterleiten		Löschen

Serverzeit: 15:11:42 14/02/2023


";

pub const ATTACK_RAM_CATAPULT: &str = "



Übersichten

Karte

Berichte

Nachrichten



Rangliste
(797.|44.968 P)

      Stamm

Profil

 300

Einstellungen




0x107 | Castiana 	(334|653) K63

	12405 		11612 		29043 		50675 		4579/4904


Berichte
Alle
Angriffe
Verteidigung
Unterstützung
Handel
Events
Sonstiges
Weitergeleitet
Öffentlich
Filter

Weiterleiten		Löschen
Betreff 	Askanon (63:35:16 // - AggresivGameplay (356|631) K63) greift 0x001 | Cheyenne Mountain (336|657) K63 an
Kampfzeit 	13.02.23 13:21:06:500
Askanon hat gewonnen
Angreiferglück
Pech

	Glück 	1.5%
Moral: 48%
Angreifer: 	Askanon
Herkunft: 	63:35:16 // - AggresivGameplay (356|631) K63

Anzahl: 	0	0	5792	0	3170	0	315	50	0	0
Verluste: 	0	0	89	0	48	0	5	1	0	0

Verteidiger: 	9Lukas5
Ziel: 	0x001 | Cheyenne Mountain (336|657) K63

Anzahl: 	16	33	0	703	9	9	50	100	0	2
Verluste: 	16	33	0	703	9	9	50	100	0	2

Schaden durch Rammböcke: 	Wall beschädigt von Level 20 auf Level 0
Schaden durch Katapultbeschuss: 	Bauernhof beschädigt von Level 30 auf Level 29

Hinweis:
Deine Einheiten wurden von deinem Gegner erspäht.

» Diesen Bericht veröffentlichen
Weiterleiten		Löschen

Serverzeit: 15:26:14 14/02/2023


";
