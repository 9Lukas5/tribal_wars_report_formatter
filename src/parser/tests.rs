
use crate::parser::parse_report;
use crate::parser::tests::input_data::*;
use crate::types::{Building, CatapultDamage, RamDamage};

pub
mod input_data;

#[test]
fn subject()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!("el Dante (| Agent Orange | (346|652) K63) greift 0x008 | Latona (342|657) K63 an", report.subject);
}

#[test]
fn battle_time()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!("13.02.23 13:25:32:865", report.battle_time);
}

#[test]
fn luck_positive()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!("5.9%", report.luck);
}

#[test]
fn luck_negative()
{
    let report = parse_report(ATTACK_EFFECTS_ATTACK_FLAG_PALADIN).unwrap();
    assert_eq!("-10.4%", report.luck);
}

#[test]
fn moral()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!("60%", report.moral);
}

#[test]
fn attacker()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!("el Dante", report.attacker);
}

#[test]
fn source()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!("346|652", report.source);
}

#[test]
fn attacker_troops()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!(vec![0, 0, 3629, 15, 1589, 0, 250, 81, 0, 0], report.attacker_troops);
}

#[test]
fn attacker_losses()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!(vec![0, 0, 3629, 15, 1589, 0, 250, 81, 0, 0], report.attacker_losses);
}

#[test]
fn defender()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!("9Lukas5", report.defender);
}

#[test]
fn target()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!("342|657", report.target);
}

#[test]
fn defender_troops()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!(vec![5330, 5078, 0, 189, 0, 494, 0, 75, 1, 0], report.defender_troops.unwrap());
}

#[test]
fn defender_losses()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!(vec![1307, 1245, 0, 46, 0, 121, 0, 18, 0, 0], report.defender_losses.unwrap());
}

#[test]
fn defender_troops_unknown()
{
    let report = parse_report(SCOUT_REPORT_NO_SURVIVERS).unwrap();
    assert_eq!(None, report.defender_losses);
}

#[test]
fn defender_losses_unknown()
{
    let report = parse_report(SCOUT_REPORT_NO_SURVIVERS).unwrap();
    assert_eq!(None, report.defender_losses);
}

#[test]
fn agreement()
{
    let report = parse_report(ATTACK_NIGHT_SCOUT_AGREEMENT).unwrap();
    assert_eq!((74, 51), report.agreement.unwrap());
}

#[test]
fn attacker_flag()
{
    let report = parse_report(ATTACK_EFFECTS_ATTACK_FLAG_PALADIN).unwrap();
    assert_eq!("+3% Angriffsstärke", report.attacker_flag.unwrap());
}

#[test]
fn defender_flag()
{
    let report = parse_report(ATTACK_NIGHT_SCOUT_AGREEMENT).unwrap();
    assert_eq!("+6% Verteidigungsstärke", report.defender_flag.unwrap());
}

#[test]
fn night_bonus_on()
{
    let report = parse_report(ATTACK_NIGHT_SCOUT_AGREEMENT).unwrap();
    assert_eq!(true, report.night_bonus);
}

#[test]
fn night_bonus_off()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert_eq!(false, report.night_bonus);
}

#[test]
fn attacker_effects()
{
    let expected = String::from("Axtkämpfer: +30% Angriffsstärke")
        + " Leichte Kavallerie: +30% Angriffsstärke"
        + " Katapult: +5% Gebäudeschaden";
    let report = parse_report(ATTACK_EFFECTS_ATTACK_FLAG_PALADIN).unwrap();

    assert_eq!(expected, report.attacker_effects.unwrap());
}

#[test]
fn defender_effects()
{
    let expected = String::from("Schwertkämpfer: +15% Verteidigungsstärke")
        + " Speerträger: +30% Verteidigungsstärke"
        + " +50% Verteidigungsstärke von Gebäuden gegen Belagerung";
    let report = parse_report(ATTACK_EFFECTS_ATTACK_FLAG_PALADIN).unwrap();

    assert_eq!(expected, report.defender_effects.unwrap());
}

#[test]
fn defender_effects_if_no_troops_found()
{
    let expected = String::from("Schwertkämpfer: +30% Verteidigungsstärke")
        + " Speerträger: +30% Verteidigungsstärke"
        + " +3% Wall-Wirkung"
        + " +50% Verteidigungsstärke von Gebäuden gegen Belagerung";
    let report = parse_report(SCOUT_REPORT_NO_SURVIVERS).unwrap();

    assert_eq!(expected, report.defender_effects.unwrap());
}

#[test]
fn paladin_experience_not_exists()
{
    let report = parse_report(SCOUT_REPORT_NO_SURVIVERS).unwrap();

    assert!(report.paladin_experience_gain.is_none());
}

#[test]
fn paladin_experience_exists()
{
    let report = parse_report(ATTACK_EFFECTS_ATTACK_FLAG_PALADIN).unwrap();

    assert!(report.paladin_experience_gain.is_some());
}

#[test]
fn paladin_experience()
{
    let expected = vec!
    [
        (String::from("Damon"), String::from("686,380 XP")),
        (String::from("Bonnie"), String::from("686,380 XP")),
    ];
    let report = parse_report(ATTACK_EFFECTS_ATTACK_FLAG_PALADIN).unwrap();

    assert_eq!(expected, report.paladin_experience_gain.unwrap());
}

#[test]
fn ram_damage()
{
    let report = parse_report(ATTACK_RAM_CATAPULT).unwrap();

    assert_eq!(RamDamage {from: 20, to: 0}, report.ram_damage.unwrap());
}

#[test]
fn catapult_damage()
{
    let report = parse_report(ATTACK_RAM_CATAPULT).unwrap();

    assert_eq!(
        CatapultDamage { building: String::from("Bauernhof"), from: 30, to: 29 },
        report.catapult_damage.unwrap());
}

#[test]
fn note()
{
    let report = parse_report(ATTACK_RAM_CATAPULT).unwrap();

    assert_eq!(
        CatapultDamage { building: String::from("Bauernhof"), from: 30, to: 29 },
        report.catapult_damage.unwrap());
}

#[test]
fn scouting_not_exists()
{
    let report = parse_report(ATTACK_REPORT).unwrap();
    assert!(report.scouting.is_none());
}

#[test]
fn scouting_exists()
{
    let report = parse_report(ATTACK_NIGHT_SCOUT_AGREEMENT).unwrap();
    assert!(report.scouting.is_some());
}

#[test]
fn scouting_resources()
{
    let expected = (String::from("112.892"), String::from("80.254"), String::from("1.175"));
    let report = parse_report(ATTACK_NIGHT_SCOUT_AGREEMENT).unwrap();
    assert_eq!(expected, report.scouting.unwrap().resources.unwrap());
}

#[test]
fn scouting_buildings()
{
    let sort_buildings =
        |
            first_building: &Building,
            second_building: &Building
        |
        {
            first_building.name().cmp(second_building.name())
        };

    let expected: Vec<Building> = vec!
    [
        Building::try_from(("Adelshof", 1)).unwrap(),
        Building::try_from(("Bauernhof", 28)).unwrap(),
        Building::try_from(("Eisenmine", 26)).unwrap(),
        Building::try_from(("Hauptgebäude", 22)).unwrap(),
        Building::try_from(("Holzfällerlager", 26)).unwrap(),
        Building::try_from(("Kaserne", 17)).unwrap(),
        Building::try_from(("Lehmgrube", 25)).unwrap(),
        Building::try_from(("Marktplatz", 15)).unwrap(),
        Building::try_from(("Schmiede", 20)).unwrap(),
        Building::try_from(("Speicher", 24)).unwrap(),
        Building::try_from(("Stall", 19)).unwrap(),
        Building::try_from(("Statue", 1)).unwrap(),
        Building::try_from(("Versammlungsplatz", 1)).unwrap(),
        Building::try_from(("Werkstatt", 4)).unwrap(),
    ];

    let mut report = parse_report(ATTACK_NIGHT_SCOUT_AGREEMENT).unwrap();
    report.scouting.as_mut().unwrap().buildings.as_mut().unwrap().sort_by(sort_buildings);

    assert_eq!(
        expected,
        report.scouting.unwrap().buildings.unwrap(),
    );
}

#[test]
fn scouting_ext_troops()
{
    let expected: Vec<u32> = vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let report = parse_report(ATTACK_NIGHT_SCOUT_AGREEMENT).unwrap();
    assert_eq!(expected, report.scouting.unwrap().ext_troops.unwrap());
}
