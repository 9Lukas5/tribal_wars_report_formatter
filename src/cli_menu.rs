use std::fmt::{Display, Formatter};
use clap::{Arg, ArgAction, ArgMatches, Command, crate_version};

#[cfg(test)]
mod tests;

/// CLI-Argument IDs
///
/// # Description
/// Enumeration for all possible arguments in the CLI-menu to have compiler checks
/// on retrieving an argument from the matches by its ID.
///
pub enum ArgumentIds
{
    Infile,
    NoOut,
    Outfile,
    Print,
    Raw,
    Verbosity,
}

impl ArgumentIds
{
    pub fn as_str(&self) -> &'static str
    {
        use ArgumentIds::*;

        match *self
        {
            Infile => "Infile",
            NoOut => "NoOut",
            Outfile => "Outfile",
            Print => "Print",
            Raw => "Raw",
            Verbosity => "Verbosity",
        }
    }
}

impl Display for ArgumentIds
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "ArgumentIds::{}", self.as_str())
    }
}

/// Definition of the binary crate's CLI-interface
pub(crate) fn build_cli_menu() -> ArgMatches
{
    Command::new("TribalWars report formatter")
        .author("9Lukas5@mastodontech.de")
        .about("This uses the text copied from a tribal wars report \
                    and generates a forum applicable bb-code formatted output\n\
                    \n\
                    The input can be simply doing CTRL+A on the open report page,\
                    but the at least needed input has to begin with the \"Betreff\" and end including \
                    the line \"Diesen Bericht veröffentlichen\"")
        .version(crate_version!())
        .arg(Arg::new(ArgumentIds::Outfile.as_str())
            .value_name("FILE")
            .short('o')
            .long("out")
            .help("Path for output file. If not set name is retrieved from the report date"))
        .arg(Arg::new(ArgumentIds::NoOut.as_str())
            .long("no-out")
            .action(ArgAction::SetTrue)
            .help("Do not output BBCode to a file")
            .conflicts_with(ArgumentIds::Outfile.as_str()))
        .arg(Arg::new(ArgumentIds::Print.as_str())
            .short('p')
            .long("print")
            .action(ArgAction::SetTrue)
            .help("Output the Forum BBCode directly on the terminal"))
        .arg(Arg::new(ArgumentIds::Raw.as_str())
            .value_name("CONTENT")
            .long("raw")
            .help("copied content from report directly given as argument on the console"))
        .arg(Arg::new(ArgumentIds::Infile.as_str())
            .value_name("FILE")
            .conflicts_with(ArgumentIds::Raw.as_str())
            .help("textfile with the copied text from the report"))
        .arg(Arg::new(ArgumentIds::Verbosity.as_str())
            .short('v')
            .long("verbose")
            .help("verbosity level: 0-Off, 1-Error, 2-Warn, 3-Info, 4-Debug, 5-Trace")
            .action(ArgAction::Count)
            .default_value("2"))
        .get_matches()
}
