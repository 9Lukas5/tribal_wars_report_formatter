
use assert_cmd::prelude::*;
use predicates::prelude::*;

use crate::bin_tests::get_cmd;

#[test]
fn crash_with_positional_and_explicit_infile() -> Result<(), Box<dyn std::error::Error>>
{
    let mut cmd = get_cmd()?;

    cmd.arg("--raw").arg("raw-input")
        .arg("positional/file/path");

    cmd.assert()
        .failure()
        .stderr(predicate::str::contains("the argument '--raw <CONTENT>' cannot be used with '[FILE]'"));

    Ok(())
}

#[test]
fn crash_with_no_out_and_out_both_given() -> Result<(), Box<dyn std::error::Error>>
{
    let mut cmd = get_cmd()?;

    cmd.arg("--no-out")
        .arg("--out").arg("raw-input");

    cmd.assert()
        .failure()
        .stderr(predicate::str::contains("the argument '--no-out' cannot be used with '--out <FILE>'"));

    Ok(())
}
