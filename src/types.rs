use std::error::Error;
use std::fmt::{Display, Formatter};
use crate::bbcode;
use crate::parser;

/// TribalWars report
///
/// # Description
///
/// This struct represents nearly all data a battle report of the german TribalWars browser game
/// can include.
///
///
/// # Usage
///
/// New empty instance:
/// ```
/// use tribal_wars_report_formatter::types::Report;
///
/// let report = Report::new();
/// ```
///
/// Create instance from String data:
/// ```
/// use tribal_wars_report_formatter::types::Report;
///
/// let report = Report::try_from("<input data>");
/// ```
///
/// Generate forum BB-Code from already parsed instance:
/// ```
/// use tribal_wars_report_formatter::types::Report;
///
/// let report = Report::new();
/// let bbcode = report.to_bbcode();
/// ```
///
#[derive(Debug)]
pub struct Report
{
    pub agreement: Option<(u32, u32)>,
    pub attacker: String,
    pub attacker_effects: Option<String>,
    pub attacker_flag: Option<String>,
    pub attacker_losses: Vec<u32>,
    pub attacker_troops: Vec<u32>,
    pub battle_time: String,
    pub catapult_damage: Option<CatapultDamage>,
    pub defender: String,
    pub defender_effects: Option<String>,
    pub defender_flag: Option<String>,
    pub defender_losses: Option<Vec<u32>>,
    pub defender_troops: Option<Vec<u32>>,
    pub luck: String,
    pub moral: String,
    pub night_bonus: bool,
    pub note: String,
    pub paladin_experience_gain: Option<Vec<(String, String)>>,
    pub ram_damage: Option<RamDamage>,
    pub report_type: ReportType,
    pub scouting: Option<Scouting>,
    pub source: String,
    pub subject: String,
    pub target: String,
    pub winner_string: String,
}

impl Report
{
    /// Creates a new instance with empty defaults
    pub fn new() -> Report
    {
        Report
        {
            agreement: None,
            attacker: String::new(),
            attacker_effects: None,
            attacker_flag: None,
            attacker_losses: vec![],
            attacker_troops: vec![],
            battle_time: String::new(),
            catapult_damage: None,
            defender: String::new(),
            defender_effects: None,
            defender_flag: None,
            defender_losses: None,
            defender_troops: None,
            luck: String::new(),
            moral: String::new(),
            night_bonus: false,
            note: String::new(),
            paladin_experience_gain: None,
            ram_damage: None,
            report_type: ReportType::Undefined,
            scouting: None,
            source: String::new(),
            subject: String::new(),
            target: String::new(),
            winner_string: String::new(),
        }
    }

    /// Generates the TribalWars in-game forum BB-Code representing this instances data
    ///
    /// # Errors
    ///
    /// If anything unexpected happens an instance of [ReportParserError] is returned.
    ///
    pub fn to_bbcode(&self) -> Result<String, ReportParserError>
    {
        bbcode::report_to_bbcode(self)
    }
}

impl TryFrom<&str> for Report
{
    type Error = ReportParserError;

    /// Creates a new instance from a [&str]
    ///
    /// # Description
    ///
    /// The string has to begin latest with the word "Betreff"
    /// and must not end before including the "Diesen Bericht veröffentlichen"
    /// of the in-game report representation.
    /// These two are the begin and end markers the parser searches for.
    ///
    /// # Errors
    ///
    /// For everything that could go wrong during the parsing an [ReportParserError] is returned.
    ///
    fn try_from(value: &str) -> Result<Self, Self::Error>
    {
        return parser::parse_report(value);
    }
}

/// Represents the data of a report with ram damage
///
/// # Description
///
/// Normally not useful to be used on its own, this is thought to be stored in a [Report] instance
/// if ram damage results are present.
///
#[derive(Debug)]
#[derive(PartialEq)]
pub struct RamDamage
{
    pub from: u32,
    pub to: u32
}

/// Represents the data of a report with catapult damage
///
/// # Description
///
/// Normally not useful to be used on its own, this is thought to be stored in a [Report] instance
/// if catapult damage results are present.
///
#[derive(Debug)]
#[derive(PartialEq)]
pub struct CatapultDamage
{
    pub building: String,
    pub from: u32,
    pub to: u32,
}

/// Reprents the data of a report with scouting results.
///
/// # Description
///
/// Normally not useful to be used on its own, this is thought to be stored in a [Report] instance
/// if scouting results are present.
///
#[derive(Debug)]
pub struct Scouting
{
    pub buildings: Option<Vec<Building>>,
    pub ext_troops: Option<Vec<u32>>,
    pub resources: Option<(String, String, String)>,
}

impl Scouting
{
    /// Creates a new instance with all fields being [None]
    pub fn new() -> Scouting
    {
        Scouting
        {
            buildings: None,
            ext_troops: None,
            resources: None,
        }
    }
}

#[derive(Debug)]
#[derive(PartialEq)]
pub enum Building
{
    Main(u32),
    Barracks(u32),
    Stable(u32),
    Garage(u32),
    Snob(u32),
    Smith(u32),
    Place(u32),
    Statue(u32),
    Market(u32),
    Wood(u32),
    Stone(u32),
    Iron(u32),
    Farm(u32),
    Storage(u32),
    Wall(u32),
}

impl Building
{
    pub fn name(&self) -> &'static str
    {
        use Building::*;

        match self
        {
            Main(_) => "Hauptgebäude",
            Barracks(_) => "Kaserne",
            Stable(_) => "Stall",
            Garage(_) => "Werkstatt",
            Snob(_) => "Adelshof",
            Smith(_) => "Schmiede",
            Place(_) => "Versammlungsplatz",
            Statue(_) => "Statue",
            Market(_) => "Marktplatz",
            Wood(_) => "Holzfällerlager",
            Stone(_) => "Lehmgrube",
            Iron(_) => "Eisenmine",
            Farm(_) => "Bauernhof",
            Storage(_) => "Speicher",
            Wall(_) => "Wall",
        }
    }

    pub fn level(&self) -> &u32
    {
        use Building::*;

        match self
        {
            Main(lvl) => lvl,
            Barracks(lvl) => lvl,
            Stable(lvl) => lvl,
            Garage(lvl) => lvl,
            Snob(lvl) => lvl,
            Smith(lvl) => lvl,
            Place(lvl) => lvl,
            Statue(lvl) => lvl,
            Market(lvl) => lvl,
            Wood(lvl) => lvl,
            Stone(lvl) => lvl,
            Iron(lvl) => lvl,
            Farm(lvl) => lvl,
            Storage(lvl) => lvl,
            Wall(lvl) => lvl,
        }
    }
}

impl Display for Building
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{}: {}", self.name(), self.level())
    }
}

impl TryFrom<(&str, u32)> for Building
{
    type Error = ReportParserError;

    fn try_from((name, lvl): (&str, u32)) -> Result<Self, Self::Error>
    {
        use Building::*;

        match name
        {
            "Hauptgebäude" => Ok(Main(lvl)),
            "Kaserne" => Ok(Barracks(lvl)),
            "Stall" => Ok(Stable(lvl)),
            "Werkstatt" => Ok(Garage(lvl)),
            "Adelshof" => Ok(Snob(lvl)),
            "Schmiede" => Ok(Smith(lvl)),
            "Versammlungsplatz" => Ok(Place(lvl)),
            "Statue" => Ok(Statue(lvl)),
            "Marktplatz" => Ok(Market(lvl)),
            "Holzfällerlager" => Ok(Wood(lvl)),
            "Lehmgrube" => Ok(Stone(lvl)),
            "Eisenmine" => Ok(Iron(lvl)),
            "Bauernhof" => Ok(Farm(lvl)),
            "Speicher" => Ok(Storage(lvl)),
            "Wall" => Ok(Wall(lvl)),
            _ => Err(ReportParserError::UnknownBuilding(name.to_string()))
        }
    }
}

impl TryFrom<(&String, u32)> for Building
{
    type Error = ReportParserError;

    fn try_from(value: (&String, u32)) -> Result<Self, Self::Error>
    {
        Building::try_from((value.0.as_str(), value.1))
    }
}

/// Used as state during parsing
pub enum SearchState
{
    Agreement,
    Attacker,
    AttackerEffects,
    AttackerFlag,
    AttackerLosses,
    AttackerTroops,
    BattleTime,
    CatapultDamage,
    Defender,
    DefenderEffects,
    DefenderFlag,
    DefenderLosses,
    DefenderTroops,
    Finished,
    Luck,
    Moral,
    NightBonus,
    PaladinExp,
    RamDamage,
    Scouting,
    ScoutingBuildings,
    ScoutingResources,
    ScoutingTroops,
    Source,
    Start,
    Subject,
    Target,
    WinnerString,
}

impl SearchState
{
    pub fn as_str(&self) -> &'static str
    {
        use SearchState::*;

        match *self
        {
            Agreement => "Agreement",
            Attacker => "Attacker",
            AttackerEffects => "AttackerEffects",
            AttackerFlag => "AttackerFlag",
            AttackerLosses => "AttackerLosses",
            AttackerTroops => "AttackerTroops",
            BattleTime => "BattleTime",
            CatapultDamage => "CatapultDamage",
            Defender => "Defender",
            DefenderEffects => "DefenderEffects",
            DefenderFlag => "DefenderFlag",
            DefenderLosses => "DefenderLosses",
            DefenderTroops => "DefenderTroops",
            Finished => "Finished",
            Luck => "Luck",
            Moral => "Moral",
            NightBonus => "NightBonus",
            PaladinExp => "PaladinExp",
            RamDamage => "RamDamage",
            Scouting => "Scouting",
            ScoutingBuildings => "ScoutingBuildings",
            ScoutingResources => "ScoutingResources",
            ScoutingTroops => "ScoutingTroops",
            Source => "Source",
            Start => "Start",
            Subject => "Subject",
            Target => "Target",
            WinnerString => "WinnerString",
        }
    }
}

impl Display for SearchState
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "SearchState::{}", self.as_str())
    }
}

/// Defines the type of battle report being found for a [Report]
#[derive(Debug)]
pub enum ReportType
{
    Attack,
    Nobilitation,
    Scout,
    Undefined,
}

/// Custom Error for this library
///
/// # Description
///
/// This is to allow a more detailed differentiation what went wrong, while having
/// a global type to be used as return type and still be easily able to match
/// on different types without the danger of spelling mistakes on matching messages.
#[derive(Debug)]
pub enum ReportParserError
{
    General(String),
    IncompleteInput,
    ParseCoordinatesError,
    ParseTroopsError,
    PatternNotFound(String),
    UnexpectedState,
    UnknownBuilding(String),
    UnknownReportType,
}

impl ReportParserError
{
    fn to_display_string(&self) -> String
    {
        use ReportParserError::*;

        match self
        {
            General(msg) => String::from("General, caused by: ") + msg.as_str(),
            IncompleteInput => String::from("IncompleteInput"),
            ParseCoordinatesError => String::from("ParseCoordinatesError"),
            ParseTroopsError => String::from("ParseTroopsError"),
            PatternNotFound(msg) => String::from("PatternNotFound, caused by: ") + msg.as_str(),
            UnexpectedState => String::from("UnexpectedState"),
            UnknownBuilding(msg) => String::from("UnknownBuilding, caused by: ") + msg.as_str(),
            UnknownReportType => String::from("UnknownReportType"),
        }
    }
}

impl Display for ReportParserError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "ReportParserError: {}", self.to_display_string())
    }
}

impl Error for ReportParserError {}
