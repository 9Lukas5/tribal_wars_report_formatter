use crate::parser::tests::input_data;
use crate::types::Report;

#[test]
fn bbcode_attack_report()
{
    let expected = ATTACK_REPORT_BBCODE;
    let report = Report::try_from(input_data::ATTACK_REPORT).unwrap();

    assert_eq!(expected, report.to_bbcode().unwrap());
}

#[test]
fn bbcode_scout_report_no_survivers()
{
    let expected = SCOUT_REPORT_NO_SURVIVERS_BBCODE;
    let report = Report::try_from(input_data::SCOUT_REPORT_NO_SURVIVERS).unwrap();

    assert_eq!(expected, report.to_bbcode().unwrap());
}

#[test]
fn bbcode_attack_night_scout_agreement()
{
    let expected = ATTACK_NIGHT_SCOUT_AGREEMENT_BBCODE;
    let report = Report::try_from(input_data::ATTACK_NIGHT_SCOUT_AGREEMENT).unwrap();

    assert_eq!(expected, report.to_bbcode().unwrap());
}

#[test]
fn bbcode_effects_attack_flag_paladin()
{
    let expected = ATTACK_EFFECTS_ATTACK_FLAG_PALADIN_BBCODE;
    let report = Report::try_from(input_data::ATTACK_EFFECTS_ATTACK_FLAG_PALADIN).unwrap();

    assert_eq!(expected, report.to_bbcode().unwrap());
}

#[test]
fn bbcode_attack_ram_catapult()
{
    let expected = ATTACK_RAM_CATAPULT_BBCODE;
    let report = Report::try_from(input_data::ATTACK_RAM_CATAPULT).unwrap();

    assert_eq!(expected, report.to_bbcode().unwrap());
}

const ATTACK_REPORT_BBCODE: &str = "[spoiler=13.02.23 13:25:32:865 - el Dante (| Agent Orange | (346|652) K63) greift 0x008 | Latona (342|657) K63 an]
Kampzeit: 13.02.23 13:25:32:[size=7][color=#868686]865[/color][/size]
[b][size=15]9Lukas5 hat gewonnen[/size][/b]

[b][i]Angreiferglück[/i][/b]: [b][color=#00a500]5.9%[/color][/b]

[b][i]Moral[/i]: 60%[/b]
[table]
[**]Angreifer:[|][player]el Dante[/player][/**]
[*]Herkunft:[|][coord]346|652[/coord][/table][table]
[*][|][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]Anzahl:[|]0[|]0[|]3629[|]15[|]1589[|]0[|]250[|]81[|]0[|]0
[*]Verluste:[|]0[|]0[|]3629[|]15[|]1589[|]0[|]250[|]81[|]0[|]0
[/table]
[table]
[**]Verteidiger:[|][player]9Lukas5[/player][/**]
[*]Ziel:[|][coord]342|657[/coord][/table][table]
[*][|][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]Anzahl:[|]5330[|]5078[|]0[|]189[|]0[|]494[|]0[|]75[|]1[|]0
[*]Verluste:[|]1307[|]1245[|]0[|]46[|]0[|]121[|]0[|]18[|]0[|]0
[/table]
[table][**]Schaden durch Rammböcke:[|][b]Wall[/b] beschädigt von Level [b]18[/b] auf Level [b]15[/b][**]Schaden durch Katapultbeschuss:[|][b]Wall[/b] beschädigt von Level [b]15[/b] auf Level [b]14[/b][/table]
[table]
[**][unit]knight[/unit] [b]Bonnie[/b][|]+ 380,130 XP[/table]

[/spoiler]";

const SCOUT_REPORT_NO_SURVIVERS_BBCODE: &str = "[spoiler=12.02.23 12:42:07:448 - 9Lukas5 (0x001 | Cheyenne Mountain (336|657) K63) späht York (344|659) K63]
Kampzeit: 12.02.23 12:42:07:[size=7][color=#868686]448[/color][/size]
[b][size=15]staublurch hat gewonnen[/size][/b]

[b][i]Angreiferglück[/i][/b]: [b][color=#00a500]16.0%[/color][/b]

[b][i]Moral[/i]: 100%[/b]
[table]
[**]Angreifer:[|][player]9Lukas5[/player][/**]
[*]Herkunft:[|][coord]336|657[/coord][/table][table]
[*][|][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]Anzahl:[|]0[|]0[|]0[|]20[|]0[|]0[|]0[|]0[|]0[|]0
[*]Verluste:[|]0[|]0[|]0[|]20[|]0[|]0[|]0[|]0[|]0[|]0
[/table]
[table]
[**]Verteidiger:[|][player]staublurch (gelöscht)[/player][/**]
[*]Ziel:[|][coord]344|659[/coord][*][|]Keiner deiner Kämpfer ist lebend zurückgekehrt. Es konnten keine Informationen über die Truppenstärke des Feindes erlangt werden.[/table][table]
[*]Flagge:[|]+8% Verteidigungsstärke
[*]Effekte:[|]Schwertkämpfer: +30% Verteidigungsstärke Speerträger: +30% Verteidigungsstärke +3% Wall-Wirkung +50% Verteidigungsstärke von Gebäuden gegen Belagerung[/table]

[/spoiler]";

const ATTACK_NIGHT_SCOUT_AGREEMENT_BBCODE: &str = "[spoiler=10.02.23 00:42:55:313 - 9Lukas5 (0x001 | Cheyenne Mountain (336|657) K63) greift Hastings (343|660) K63 an]
Kampzeit: 10.02.23 00:42:55:[size=7][color=#868686]313[/color][/size]
[b][size=15]9Lukas5 hat gewonnen[/size][/b]

[b][i]Angreiferglück[/i][/b]: [b][color=#00a500]10.5%[/color][/b]

[b][i]Moral[/i]: 100%[/b]
[b][i]Nachtbonus für Verteidiger[/i][/b][table]
[**]Angreifer:[|][player]9Lukas5[/player][/**]
[*]Herkunft:[|][coord]336|657[/coord][/table][table]
[*][|][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]Anzahl:[|]0[|]0[|]0[|]20[|]0[|]300[|]0[|]0[|]0[|]1
[*]Verluste:[|]0[|]0[|]0[|]0[|]0[|]33[|]0[|]0[|]0[|]0
[/table]
[table]
[**]Verteidiger:[|][player]staublurch (gelöscht)[/player][/**]
[*]Ziel:[|][coord]343|660[/coord][/table][table]
[*][|][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]Anzahl:[|]18[|]42[|]0[|]0[|]0[|]22[|]0[|]0[|]0[|]0
[*]Verluste:[|]18[|]42[|]0[|]0[|]0[|]22[|]0[|]0[|]0[|]0
[/table][table]
[*]Flagge:[|]+6% Verteidigungsstärke
[/table]
[b][i]Spionage[/i][/b][table][**]Erspähte Rohstoffe:[|][building]wood[/building] 112.892 [building]stone[/building] 80.254 [building]iron[/building] 1.175[/table][table][**]Gebäude[||]Stufe[/**]
[*][building]main[/building] Hauptgebäude[|]22
[*][building]barracks[/building] Kaserne[|]17
[*][building]stable[/building] Stall[|]19
[*][building]garage[/building] Werkstatt[|]4
[*][building]snob[/building] Adelshof[|]1
[*][building]smith[/building] Schmiede[|]20
[*][building]place[/building] Versammlungsplatz[|]1
[*][building]statue[/building] Statue[|]1
[*][building]market[/building] Marktplatz[|]15
[*][building]wood[/building] Holzfällerlager[|]26
[*][building]stone[/building] Lehmgrube[|]25
[*][building]iron[/building] Eisenmine[|]26
[*][building]farm[/building] Bauernhof[|]28
[*][building]storage[/building] Speicher[|]24
[/table]
[b]Einheiten außerhalb:[/b]
[table][*][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]0[|]0[|]0[|]0[|]0[|]0[|]0[|]0[|]0[|]0[/table]
[table][**]Zustimmung:[|]Gesunken von [b]74[/b] auf [b]51[/b][/table]
[/spoiler]";

const ATTACK_EFFECTS_ATTACK_FLAG_PALADIN_BBCODE: &str = "[spoiler=13.02.23 12:23:13:710 - Askanon (63:35:07 // - AggresivGameplay (357|630) K63) greift 0x008 | Latona (342|657) K63 an]
Kampzeit: 13.02.23 12:23:13:[size=7][color=#868686]710[/color][/size]
[b][size=15]9Lukas5 hat gewonnen[/size][/b]

[b][i]Angreiferglück[/i][/b]: [b][color=#ff0000]-10.4%[/color][/b]

[b][i]Moral[/i]: 48%[/b]
[table]
[**]Angreifer:[|][player]Askanon[/player][/**]
[*]Herkunft:[|][coord]357|630[/coord][/table][table]
[*][|][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]Anzahl:[|]0[|]0[|]6260[|]0[|]3122[|]0[|]314[|]50[|]0[|]0
[*]Verluste:[|]0[|]0[|]6260[|]0[|]3122[|]0[|]314[|]50[|]0[|]0
[/table][table]
[*]Flagge:[|]+3% Angriffsstärke
[*]Effekte:[|]Axtkämpfer: +30% Angriffsstärke Leichte Kavallerie: +30% Angriffsstärke Katapult: +5% Gebäudeschaden[/table]
[table]
[**]Verteidiger:[|][player]9Lukas5[/player][/**]
[*]Ziel:[|][coord]342|657[/coord][/table][table]
[*][|][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]Anzahl:[|]9110[|]8670[|]0[|]211[|]0[|]1587[|]0[|]84[|]2[|]0
[*]Verluste:[|]945[|]900[|]0[|]22[|]0[|]165[|]0[|]9[|]0[|]0
[/table][table]
[*]Effekte:[|]Schwertkämpfer: +15% Verteidigungsstärke Speerträger: +30% Verteidigungsstärke +50% Verteidigungsstärke von Gebäuden gegen Belagerung[/table]
[table][**]Schaden durch Rammböcke:[|][b]Wall[/b] beschädigt von Level [b]18[/b] auf Level [b]17[/b][/table]
[table]
[**][unit]knight[/unit] [b]Damon[/b][|]+ 686,380 XP
[**][unit]knight[/unit] [b]Bonnie[/b][|]+ 686,380 XP[/table]

[/spoiler]";

const ATTACK_RAM_CATAPULT_BBCODE: &str = "[spoiler=13.02.23 13:21:06:500 - Askanon (63:35:16 // - AggresivGameplay (356|631) K63) greift 0x001 | Cheyenne Mountain (336|657) K63 an]
Kampzeit: 13.02.23 13:21:06:[size=7][color=#868686]500[/color][/size]
[b][size=15]Askanon hat gewonnen[/size][/b]

[b][i]Angreiferglück[/i][/b]: [b][color=#00a500]1.5%[/color][/b]

[b][i]Moral[/i]: 48%[/b]
[table]
[**]Angreifer:[|][player]Askanon[/player][/**]
[*]Herkunft:[|][coord]356|631[/coord][/table][table]
[*][|][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]Anzahl:[|]0[|]0[|]5792[|]0[|]3170[|]0[|]315[|]50[|]0[|]0
[*]Verluste:[|]0[|]0[|]89[|]0[|]48[|]0[|]5[|]1[|]0[|]0
[/table]
[table]
[**]Verteidiger:[|][player]9Lukas5[/player][/**]
[*]Ziel:[|][coord]336|657[/coord][/table][table]
[*][|][unit]spear[/unit][|][unit]sword[/unit][|][unit]axe[/unit][|][unit]spy[/unit][|][unit]light[/unit][|][unit]heavy[/unit][|][unit]ram[/unit][|][unit]catapult[/unit][|][unit]knight[/unit][|][unit]snob[/unit]
[*]Anzahl:[|]16[|]33[|]0[|]703[|]9[|]9[|]50[|]100[|]0[|]2
[*]Verluste:[|]16[|]33[|]0[|]703[|]9[|]9[|]50[|]100[|]0[|]2
[/table]
[table][**]Schaden durch Rammböcke:[|][b]Wall[/b] beschädigt von Level [b]20[/b] auf Level [b]0[/b][**]Schaden durch Katapultbeschuss:[|][b]Bauernhof[/b] beschädigt von Level [b]30[/b] auf Level [b]29[/b][/table]

[/spoiler]";
